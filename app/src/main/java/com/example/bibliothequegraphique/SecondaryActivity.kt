package com.example.bibliothequegraphique

import android.app.Activity
import android.os.Bundle
import android.widget.Button
import splitties.activities.start
import splitties.activities.startActivity

class SecondaryActivity: Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.second_activity_main)

        findViewById<Button>(R.id.oldActivityBtn).setOnClickListener { start<MainActivity>() }
    }
}