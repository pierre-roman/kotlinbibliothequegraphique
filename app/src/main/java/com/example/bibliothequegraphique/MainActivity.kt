package com.example.bibliothequegraphique

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import splitties.activities.start
import splitties.activities.startActivity
import splitties.alertdialog.*
import splitties.toast.toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toast(R.string.text_island)
        Toast.makeText(this, "${getString(R.string.text_island)}", Toast.LENGTH_SHORT).show()
        findViewById<Button>(R.id.mainBtn).setOnClickListener { showAlertDialog() }

        /*Ouvrir une page internet*/
        val url = "www.knightmodels.com/fr/33-justice-league/s-1/lider-agente_libre/origin-general_comic"

        findViewById<Button>(R.id.internetButton).setOnClickListener { browse(url) }

        findViewById<Button>(R.id.mailButton).setOnClickListener { sendEmail("macha.dacostabarros@ynov.com", "Hi", "Hello!") }

        findViewById<Button>(R.id.shareBtn).setOnClickListener { share(R.string.text_share, R.string.title_share) }

        findViewById<Button>(R.id.newActivityBtn).setOnClickListener { start<SecondaryActivity>() }

    }

    private fun showAlertDialog() {
        alertDialog {
            messageResource = R.string.text_alert
            okButton {
                showAlertDialog()
            }
            cancelButton()
        }.onShow {
            positiveButton.setText(R.string.action_like)
        }.show()
    }

    private fun browse(url: String)
    {
        var browser = Intent(Intent.ACTION_VIEW, Uri.parse("https://"+url))
        startActivity(browser)
    }

    private fun sendEmail(to: String, subject: String, msg: String) {
        val emailIntent = Intent(Intent.ACTION_SEND)

        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "text/plain"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        emailIntent.putExtra(Intent.EXTRA_TEXT, msg)

        try {
            startActivity(Intent.createChooser(emailIntent, getString(R.string.title_send_email)))
        } catch (ex: ActivityNotFoundException) {
            toast(R.string.text_no_email_client)
        }
    }



}