# kotlinBibliothèqueGraphique

# Guide du développement du projet

| Action dans le projet |    Étiquette    |
| :---------------      |:---------------:|
| Ajout                 |      [ADD]      |
| Supprimer             |      [DEL]      |
| Développement         |      [DEV]      |
| Correction            |      [FIX]      |
| Mise à jour de branche|      [MAJ]      |
